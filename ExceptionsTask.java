import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExceptionsTask {


    public static void main(String[] args) {
        ExceptionsTask test = new ExceptionsTask();
        test.nullException();
        test.indexoutOfBounds();
        test.numberFormatException();
        test.classCastException();
        test.classNotFoundException();
        test.unsupportedOperation();
    }

    public void nullException() {
        String str = null;
        System.out.println("Length :" + str.length());
    }

    public void indexoutOfBounds() {
        int a[] = new int[10];
        a[11] = 9;
        System.out.println("Test:" + a);
    }

    public void numberFormatException() {
        int value = Integer.parseInt("Test Number of Format Exceprion");
        System.out.println(value);

    }

    public void classCastException() {
        Object test = new Integer(8);
        System.out.println((String) test);

    }

    public void classNotFoundException() {
        Class.forName("Test");
    }

   
    public void unsupportedOperation() {
        List<String> strings = new ArrayList<>();
        List<String> unmodifiable = Collections.unmodifiableList(strings);
        unmodifiable.add("Ukraine");
        unmodifiable.add("Italy");
        strings.add("Retest");
        System.out.println(unmodifiable);
    }

}
